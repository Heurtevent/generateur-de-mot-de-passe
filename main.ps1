
[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') #Commande pour charger la bibiolothèque Windows Form

#Création Btn_Quitter
$btn_quitter = New-Object Windows.Forms.Button
$btn_quitter.location = New-Object System.Drawing.Size(317, 317)
$btn_quitter.size = New-Object System.Drawing.Size(94, 24)
$btn_quitter.text = "Quitter"
$btn_quitter.Add_Click({ #Méthode Add_Click affecté au bouton quitter
    $frm_gmpd.Close() #Lorsque l'on rentre dans 
})

#Création Btn_gen
$btn_gen = New-Object Windows.Forms.Button
$btn_gen.location = New-Object System.Drawing.Size(53, 317)
$btn_gen.size = New-Object System.Drawing.Size(94, 24)
$btn_gen.text = "Générer"
$btn_gen.Add_Click({
    $complex = 0
    $lst_caract = ""
    
    if ($case_maj.Checked) #Si la case maj est coché alors
    {
        $lst_caract = "ABCDEFGHIJKLMNOPQRSTVWYZ"
        $complex++
    }
    if ($case_min.Checked) #Si la case min est coché alors
    {
        $lst_caract = "abcdefghijklmnopqrstvwyz"
        $complex++
    }
    if ($case_ch.Checked)
    {
        $lst_caract = "0123456789"
        $complex++
    }
    if ($case_aut.Checked) #Si la case aut est coché alors
    {
        $lst_caract = "_!#&@$%?."
        $complex++
    }
    [int]$nbc = $lst_caract.Length
    if ($nbc -ne 0) 
    {
        [int]$taillemdp = $chp_nb.Text
        $mdp = ""
        $alea = New-Object Random
        for ($i=0; $i -lt $taillemdp; $i++) 
        {
            $nba = $alea.Next(0,$nbc)
            $mdp+=$lst_caract[$nba]
        } 
        $complex = $complex * ($taillemdp * 2.57)
        if($complex -gt 100){
            $complex = 100 
        }
        $br_comp.value = $complex
        $chp_mdp.Text = $mdp
     }
})

#Création Formulaire
$frm_gmpd = New-Object Windows.Forms.Form
$frm_gmpd.text = "Générateur de mots de passe"
$frm_gmpd.size = New-Object System.Drawing.Size(475, 395)

#Création Lbl_car
$lbl_car = New-Object Windows.Forms.Label
$lbl_car.location = New-Object System.Drawing.Size(8, 30) #Emplacement du label
$lbl_car.size = New-Object System.Drawing.Size(100, 20) #Taille du label
$lbl_car.text = "Caractères"

#Création Case_Maj
$case_maj = New-Object Windows.Forms.Checkbox
$case_maj.location = New-Object System.Drawing.Size(118, 30)
$case_maj.size = New-Object System.Drawing.Size(80, 20)
$case_maj.text = "Majuscules"

#Création Case_Min
$case_min = New-Object Windows.Forms.Checkbox
$case_min.location = New-Object System.Drawing.Size(208, 30)
$case_min.size = New-Object System.Drawing.Size(80, 20)
$case_min.text = "Minuscules"

#Création Case_Ch
$case_ch = New-Object Windows.Forms.Checkbox
$case_ch.location = New-Object System.Drawing.Size(298, 30)
$case_ch.size = New-Object System.Drawing.Size(80, 20)
$case_ch.text = "Chiffres"

#Création Case_Aut
$case_aut = New-Object Windows.Forms.Checkbox
$case_aut.location = New-Object System.Drawing.Size(388, 30)
$case_aut.size = New-Object System.Drawing.Size(80, 20)
$case_aut.text = "Autres"

#Création Lbl_Nbc
$lbl_nbc = New-Object Windows.Forms.label
$lbl_nbc.location = New-Object System.Drawing.Size(8, 100)
$lbl_nbc.size = New-Object System.Drawing.Size(120, 20)
$lbl_nbc.text = "Nombre de caractères"

#Création Chp_Nb
$chp_nb = New-Object Windows.Forms.TextBox
$chp_nb.location = New-Object System.Drawing.Size(150, 100)
$chp_nb.size = New-Object System.Drawing.Size(300, 20)
$chp_nb.text = "10"

#Création Lbl_Comp
$lbl_comp = New-Object Windows.Forms.Label
$lbl_comp.location = New-Object System.Drawing.Size(8, 170)
$lbl_comp.size = New-Object System.Drawing.Size(120, 20)
$lbl_comp.text = "Complexité"

#Création Br_Comp
$br_comp = New-Object Windows.Forms.ProgressBar
$br_comp.location = New-Object System.Drawing.Size(150, 160)
$br_comp.size = New-Object System.Drawing.Size(300, 40)

#Création Lbl_Mdp
$lbl_mpd = New-Object Windows.Forms.Label
$lbl_mpd.location = New-Object System.Drawing.Size(8, 240)
$lbl_mpd.size = New-Object System.Drawing.Size(120, 20)
$lbl_mpd.text = "Mot de passe"

#Création Chp_Mdp
$chp_mdp = New-Object Windows.Forms.TextBox
$chp_mdp.location = New-Object System.Drawing.Size(150, 230)
$chp_mdp.size = New-Object System.Drawing.Size(300, 40)
$chp_mdp.Multiline = $true; #Autorise le multigne de la textbox
$chp_mdp.ReadOnly = $true; #Désactive l'écriture

#Ajout des éléments dans le formulaire frm_gmpd
$frm_gmpd.Controls.Add($btn_quitter)
$frm_gmpd.Controls.Add($btn_gen)
$frm_gmpd.Controls.Add($lbl_car)
$frm_gmpd.Controls.Add($case_maj)
$frm_gmpd.Controls.Add($case_min)
$frm_gmpd.Controls.Add($case_ch)
$frm_gmpd.Controls.Add($case_aut)
$frm_gmpd.Controls.Add($lbl_nbc)
$frm_gmpd.Controls.Add($chp_nb)
$frm_gmpd.Controls.Add($Br_Comp)
$frm_gmpd.Controls.Add($lbl_comp)
$frm_gmpd.Controls.Add($lbl_mpd)
$frm_gmpd.Controls.Add($chp_mdp)

#Affiche le formulaire
$frm_gmpd.ShowDialog() #Affiche la page complète
